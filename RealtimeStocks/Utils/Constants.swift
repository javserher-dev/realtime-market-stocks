//
//  Constants.swift
//  RealtimeStocks
//
//  Created by Javier Servate on 13/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

//There is no point on writing the serverUrl every time, because if it changes in the future we must change it in every line of code it has been written. So that's the reason I store it here.
struct Constants {
    static let serverUrl = "ws://159.89.15.214:8080/"
    
    struct cells {
        static let stockCellIdentifier = "cell"
        static let stockCellNib = "StockTableViewCell"
    }
    
    struct Notifications {
        static let connected = "socketConnected"
        static let update = "stockUpdate"
        static let userInfoIndexKey = "indexUpdated"
        static let userInfoPriceDifferenceKey = "isDifferencePositive"
    }
    
    //For now we just have this VC but in case there were more, titles go here
    struct NavigationTitles {
        static let stockListTableView = "Stocks"
    }
    
    static let stockNames = ["Apple", "Google", "Amazon", "Microsoft", "Invesco", "Netflix", "Facebook", "BMW Australia", "Yorbeau Res", "Accenture"]
}

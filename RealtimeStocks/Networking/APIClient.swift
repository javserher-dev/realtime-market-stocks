//
//  APIClient.swift
//  RealtimeStocks
//
//  Created by Javier Servate on 14/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Starscream


final class APIClient: NSObject {
    
    static let shared = APIClient()
    var stocks = Stocks()
    private var socket: WebSocket?
    
    func setupWebSocket() {
        guard let url = URL(string: Constants.serverUrl) else { return }
        socket = WebSocket(url: url)
        socket?.delegate = self
        socket?.connect()
    }
    
    func subscribeTo(list: [String : Any]) {
        do {
            let data = try JSONSerialization.data(withJSONObject: list)
            socket?.write(data: data)
        } catch { print("Couldnt encode data") }
    }

}

extension APIClient: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        print("Connected")
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.connected), object: nil)
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("Disconnected")
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        guard let data = text.data(using: .utf16) else { return }
        do {
            let stock = try JSONDecoder().decode(Stock.self, from: data)
            guard let index = stocks.firstIndex(where: { $0.isin == stock.isin }) else {
                stocks.append(stock)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.update), object: nil)
                return
            }
            
            stocks[index] = stock
            
            let userInfo = [Constants.Notifications.userInfoIndexKey : index]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notifications.update), object: nil, userInfo: userInfo)
        } catch { print("Couldn't parse data") }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("Received data")
    }
}

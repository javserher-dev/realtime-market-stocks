//
//  stocksTableViewTableViewController.swift
//  RealtimeStocks
//
//  Created by Javier Servate on 13/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit
import Starscream

final class StockListTableViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.topItem?.title = Constants.NavigationTitles.stockListTableView

        setupTableView()
        registerNotifications()
        APIClient.shared.setupWebSocket()
    }
    
    private func setupTableView() {
        tableView.register(UINib.init(nibName: Constants.cells.stockCellNib, bundle: nil), forCellReuseIdentifier: Constants.cells.stockCellIdentifier)
        tableView.separatorStyle = .none
        tableView.estimatedRowHeight = 50
    }
    
    private func registerNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(subscribeToList), name: NSNotification.Name(rawValue: Constants.Notifications.connected), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(receivedUpdate(_:)), name: NSNotification.Name(rawValue: Constants.Notifications.update), object: nil)
    }
    
    @objc func subscribeToList() {
        let list = ["subscribe" : ["US0378331005", "US38259P5089", "US0231351067", "US5949181045", "BMG491BT1088", "US64110L1061", "US30303M1027", "CH0031240127", "CA9861913023", "IE00B4BNMY34"]]
        APIClient.shared.subscribeTo(list: list)
    }
    
    @objc func receivedUpdate(_ notification: Notification) {
        guard let info = notification.userInfo, let index = info[Constants.Notifications.userInfoIndexKey] as? Int else {
            tableView.reloadData()
            return
        }
        
        let indexUpdated = IndexPath(row: index, section: 0)
        tableView.reloadRows(at: [indexUpdated], with: .none)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return APIClient.shared.stocks.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.cells.stockCellIdentifier, for: indexPath) as! StockTableViewCell

        guard APIClient.shared.stocks.count > 0 else { return cell }

        cell.setupCell(index: indexPath.row, stock: APIClient.shared.stocks[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

//
//  StockTableViewCell.swift
//  RealtimeStocks
//
//  Created by Javier Servate on 14/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

final class StockTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func setupCell(index: Int, stock: Stock) {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale.current
        
        nameLabel.text = "\(Constants.stockNames[index])"
        let number = NSNumber(value: stock.price)
        guard let priceFormatted = numberFormatter.string(from: number) else { return }
        valueLabel.text = "\(priceFormatted)"
    }
}


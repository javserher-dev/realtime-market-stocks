//
//  Stock.swift
//  RealtimeStocks
//
//  Created by Javier Servate on 13/03/2019.
//  Copyright © 2019 Javier Servate. All rights reserved.
//

import UIKit

typealias Stocks = [Stock]

struct Stock: Codable {
    let isin: String
    let price, bid, ask: Double
}

# Realtime Market Stocks Prices

**Displays the market stocks realtime prices using Swift 4, Websockets (Starscream third-party framework), Reactive (NotificationCenter) and a remote server with faked data.**

**Designed, implemented and structured always following the SOLID principles to make it as much reusable, scalable and easy to maintain as possible with some patterns like MVC (design), Singleton (in the APIClient for example) and low-coupling (using a Constants.swift file for every string literal).**

This project was just to try websockets, not to achieve a good looking app. At first I did it so when the new price of the stock is higher than the previous one it shows the new price is green and the other way round, in case it is negative in red. But at the end it was overwhelming for the user so I left it with a simple black and white colours.

**It subscribes to 10 values of the stocks market and displays the prices in realtime. **
